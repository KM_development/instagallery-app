package developer.instagallery.server_requests;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by fkras on 17/03/2016.
 *
 * Classe astratta che consente la comunicazione asincrona con protocollo HTTP con metodo GET, tramite libreria OkHttp, ritornando un JSON.
 * E' necessaria la reimplementazione della funzione onPostExecute ogni volta che si utilizza questa classe.
 */
public abstract class GenericGetJSONTask extends AsyncTask<PostComponents, Void, Object> {

    private Object post(PostComponents pc) throws IOException {
        OkHttpClient client = new OkHttpClient();

        String getUrl = pc.getUrl()+"?";
        if(pc.getJsonArrString() != null) {
            for(int i=0;i<pc.getJsonArrString().size();i++) {
                getUrl += pc.getJsonArrString().get(i).first+"="+pc.getJsonArrString().get(i).second+"&";
            }
        }

        getUrl = getUrl.substring(0,getUrl.length()-1);

        Request request = new Request.Builder()
                .url(getUrl)
                .build();

        Response response = null;

        try {
            response = client.newCall(request).execute();
            JSONObject tempJsOb = new JSONObject(response.body().string());
            //Log.e("JSON RESP", tempJsOb.toString());
            return tempJsOb;
        } catch (JSONException e) {
            try {
                JSONArray tempJsArr = new JSONArray(response.body().string());
                return tempJsArr;
            } catch(JSONException e2) {
                Log.e("ERRORE2", "2");
                return null;
            } catch (IOException e2) {
                Log.e("ERRORE2", "2");
                return null;
            }
        } catch (IOException e) {
            Log.e("ERRORE", "2");
            return null;
        }
    }
    @Override
    protected Object doInBackground(PostComponents... pcs) {
        try {
            Object ret = post(pcs[0]);
            return ret;
        } catch (IOException e) {
            return null;
        }
    }


    // lascio vuota: implementare come si vuole facendo extends di GenericGetJSONArrayTask con <Specific>PostTask
    protected abstract void onPostExecute(final Object jsonResponse);

}
