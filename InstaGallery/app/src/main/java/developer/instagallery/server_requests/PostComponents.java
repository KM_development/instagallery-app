package developer.instagallery.server_requests;

import android.util.Pair;

import java.util.ArrayList;

/*
* Classe che contiene le informazioni utili per eseguire richieste tramite la libreria OhHttp, all'interno delle classi Generic<method>JSONTask
*
* */
public class PostComponents {
    private String url;
    private ArrayList<Pair<String, String>> jsonArrString;

    public PostComponents(String url, ArrayList<Pair<String, String>> jsonArrString) {
        this.url = url;
        this.jsonArrString = jsonArrString;
    }

    public String getUrl() {
        return url;
    }

    public ArrayList<Pair<String, String>> getJsonArrString() {
        return jsonArrString;
    }
}