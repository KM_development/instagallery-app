package developer.instagallery.components;

import android.app.Activity;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import developer.instagallery.MainActivity;
import developer.instagallery.R;


/**
 * Created by Filip Krasniqi on 28/03/2017.
 *
 * Adapter per visualizzazione della lista e gestione eventi su di essi.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder> {

    private ArrayList<ItemGallery> mValues;
    private MainActivity activity;

    public GalleryAdapter(ArrayList<ItemGallery> mValues, MainActivity activity) {
        this.mValues = mValues;
        this.activity = activity;
    }

    public class OpenImageOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            activity.showImage(((ImageView)v).getDrawable());
        }
    }

    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_gallery, parent, false);
        return new GalleryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GalleryViewHolder holder, int position) {
        Picasso.with(activity.getApplicationContext()).load(mValues.get(position).getFileName()).into(holder.getIcon());
        holder.getIcon().setOnClickListener(new OpenImageOnClickListener());
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class GalleryViewHolder extends RecyclerView.ViewHolder {
        private ImageView icon;

        public GalleryViewHolder(View view) {
            super(view);
            icon = (ImageView) view.findViewById(R.id.icon);
        }

        public ImageView getIcon() {
            return icon;
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }
}
