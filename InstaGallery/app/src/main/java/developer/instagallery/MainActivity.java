package developer.instagallery;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.ValueCallback;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import developer.instagallery.components.GalleryAdapter;
import developer.instagallery.components.ItemGallery;
import developer.instagallery.gui_components.BlurBuilder;
import developer.instagallery.instagram.ApplicationData;
import developer.instagallery.instagram.InstagramApp;
import developer.instagallery.server_requests.GenericGetJSONTask;
import developer.instagallery.server_requests.PostComponents;

import static developer.instagallery.instagram.InstagramApp.MEDIA_API_URL;
import static developer.instagallery.instagram.InstagramApp.USER_API_URL;

public class MainActivity extends AppCompatActivity {

    private InstagramApp mApp;                                                                      //classe che gestisce comunicazione con API di instagram
    private GalleryAdapter adapter;
    private RecyclerView list;

    private WebView mWebView;
    private ImageView imageView;
    private RelativeLayout imageContainer;
    private RelativeLayout containerLogged;
    private Snackbar snackbar;                                                                      //nel caso in cui non funzioni la connessione, lo comunico tramite snackbar

    private BroadcastReceiver onCodeReceiver;                                                       //receiver per comunicazione tra InstagramApp e l'activity: quando l'app ha scaricato il token può procedere con la visualizzazione (initInterface)

    private ProgressDialog mProgress;                                                               //loading per comunicazione con webview
    private String code;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mProgress = new ProgressDialog(this);
        mProgress.setCancelable(false);

        mApp = new InstagramApp(this, ApplicationData.CLIENT_ID,
                ApplicationData.CLIENT_SECRET, ApplicationData.CALLBACK_URL, mProgress);

        list = (RecyclerView) findViewById(R.id.list);
        mWebView = (WebView) findViewById(R.id.webview);
        imageView = (ImageView) findViewById(R.id.image);
        imageContainer = (RelativeLayout) findViewById(R.id.image_container);

        imageContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageContainer.setVisibility(View.GONE);
            }
        });

        containerLogged = (RelativeLayout)findViewById(R.id.container_logged);
        mProgress.setTitle("Loading instagram...");
        mProgress.setMessage("");

        if (mApp.hasAccessToken()) {                                                                //controllo se esiste una sessione di instagram
            initInterface();
        } else {
            //splashscreen e richiesta automatica: se non va a buon fine, snackbar...
            showWeb();
            setUpWebView();
        }

        onCodeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if(mApp.hasAccessToken()) {                                                         //controllo che sia andata a buon fine la chiamata, ovvero se ho l'accessToken
                    initInterface();
                    invalidateOptionsMenu();
                }
            }
        };
        final IntentFilter lastLocation = new IntentFilter(getString(R.string.token_received));
        registerReceiver(onCodeReceiver, lastLocation);
    }

    //toggle: visualizzo webview, non visualizzo container di grafica del login
    private void showWeb() {
        mWebView.setVisibility(View.VISIBLE);
        containerLogged.setVisibility(View.GONE);
    }

    //toggle: visualizzo solamente in contenuti, ovvero le immagini pubblicate
    private void showContents() {
        mWebView.setVisibility(View.GONE);
        imageContainer.setVisibility(View.GONE);
        containerLogged.setVisibility(View.VISIBLE);
    }

    //init interfaccia: scarico gli url delle immagini associate al profilo che si è loggato, e le rendo visibili tramite Picasso nella RecyclerView
    private void initInterface() {
        showContents();
        ArrayList<Pair<String, String>> jsonArray = new ArrayList<>();
        mProgress.setTitle("Retrieving images...");
        mProgress.show();
        GenericGetJSONTask retrieveDataTask = new GenericGetJSONTask() {
            @Override
            protected void onPostExecute(Object obj) {
                JSONObject jsonObj = (JSONObject)obj;
                JSONArray images = null;
                try {
                    images = jsonObj.getJSONArray("data");
                    ArrayList<ItemGallery> items = new ArrayList<>();
                    for(int i=0;i<images.length();i++) {
                        try {
                            items.add(new ItemGallery(images.getJSONObject(i).getJSONObject("images").getJSONObject("standard_resolution").getString("url")));
                        } catch (JSONException ex) {
                            Log.e("JSONEX", ex.getMessage());
                        }
                    }
                    adapter = new GalleryAdapter(items, MainActivity.this);
                    list.setHasFixedSize(true);
                    list.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                    list.setAdapter(adapter);
                    mProgress.dismiss();
                    //list.invalidate();
                } catch (JSONException ex) {
                    Log.e("", ex.getMessage());
                }
            }
        };
        jsonArray.add(new Pair<String, String>("access_token", mApp.getmAccessToken()));
        PostComponents pc = new PostComponents(USER_API_URL+mApp.getId()+MEDIA_API_URL, jsonArray);
        retrieveDataTask.execute(pc);
    }

    //init di webview: ricarico l'URL dopo aver impostato il listener sulle pagine
    private void setUpWebView() {
        //rimuovo cookies, altrimenti mantiene sessione anche dopo il logout (a questo punto lo faccio ogni volta che ricarico l'url)
        CookieSyncManager.createInstance(getApplicationContext());
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();

        mWebView.setVerticalScrollBarEnabled(false);
        mWebView.setHorizontalScrollBarEnabled(false);
        mWebView.setWebViewClient(new MainActivity.OAuthWebViewClient());
        mWebView.getSettings().setJavaScriptEnabled(true);
        mProgress.show();
        mWebView.loadUrl(mApp.getmAuthUrl());
    }

    private class OAuthWebViewClient extends WebViewClient {

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error){

            super.onReceivedError(view, request, error);
            view.loadUrl("about:blank");
            mWebView.setVisibility(View.GONE);
            snackbar = Snackbar.make(findViewById(R.id.activity_main), "Connessione inesistente.", Snackbar.LENGTH_INDEFINITE);
            snackbar.show();
            snackbar.setAction("Ricarica", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //riprova tutto
                    setUpWebView();
                    showWeb();
                }
            });
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if(url.startsWith(InstagramApp.mCallbackUrl)) {
                view.loadUrl("about:blank");
                showContents();
                //codice da cui ricavo l'accessToken (per il funzionamento delle API di instagram)
                code = url.split("=")[1];
                mApp.getAccessToken(code);
            } else {
                showWeb();
            }
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if(url.startsWith(InstagramApp.mCallbackUrl)) {
                mProgress.setTitle("Retrieving information from instagram...");
            } else {
                mProgress.dismiss();
                view.evaluateJavascript(
                "(function() { return ('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>'); })();",
                    new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String html) {
                            if(html.contains("403")) {
                                //login con utente che non è stato abilitato tramite sandbox invite
                                setUpWebView();
                                Toast.makeText(MainActivity.this, "L'utente non è abilitato ad utilizzare per questa applicazione!", Toast.LENGTH_LONG).show();
                            }
                        }
                });
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        //rendo visibile il menù solamente se sono loggato
        return mApp.hasAccessToken();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.logout_item) {
            //effettuo il logout
            mProgress.setTitle("Logging out...");
            mApp.resetAccessToken();
            invalidateOptionsMenu();
            //le prossime operazioni sono per evitare di vedere callbackUrl
            setUpWebView();
            showWeb();
        }
        return super.onOptionsItemSelected(item);
    }

    //visualizzazione immagine: prendo ciò che ho attorno e ne faccio un Blur
    public void showImage(Drawable drawable) {
        mWebView.setVisibility(View.GONE);
        imageContainer.setVisibility(View.VISIBLE);
        imageContainer.bringToFront();

        Bitmap b = getBitmapFromView(findViewById(R.id.activity_main));

        BlurBuilder.blur(MainActivity.this, b, imageContainer, findViewById(R.id.activity_main).getHeight());
        Bitmap bitmap = drawableToBitmap(drawable);
        imageView.setImageBitmap(bitmap);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        RelativeLayout.LayoutParams layoutParams = null;
        //al cambio della configurazione, per avere una visualizzazione ottimale dell'immagine ingrandita, ne modifico i parametri (inverto semplicemente wrap e match nei versi corretti)
        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        } else {
            layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
        imageView.setLayoutParams(layoutParams);
    }

    @Override
    public void onBackPressed() {
        if(imageContainer.isShown()) {
            //solo se l'immagine è vista ingrandita, la nascondo, altrimenti chiudo l'app
            imageContainer.setVisibility(View.GONE);
        } else super.onBackPressed();
    }

    //funzione (testata) presa da stackOverFlow per convertire drawable in bitmap
    private Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    //funzione (testata) presa da stackOverFlow per convertire ricavare immagine da una view: mi consente di fare il Blur per creare lo sfondo dinamico alla visualizzazione dell'immagine
    public static Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }
}
